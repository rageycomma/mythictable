﻿using System.Collections.Generic;

/// <summary>
/// The datatypes.
/// </summary>
namespace MythicTable.Notes.Data
{
    /// <summary>
    /// The type of the content.
    /// </summary>
    public enum MythicNoteContentType
    {
        /// <summary>
        /// "Mythic Markdown" includes markup for things like click-to-roll, and so on. (i.e. <roll>1d6+2</roll>)
        /// </summary>
        Default_MythicMarkdown,
    }

    /// <summary>
    /// Allows for the user to set different types of note (Module, chapter, monster, player, item, etc).
    /// </summary>
    public interface INoteItemType
    {
        /// <summary>
        /// The type of a note item.s
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The note item type
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// The description of the type.
        /// </summary>
        public string TypeDescription { get; set; }
    }

    /// <summary>
    /// The note item which is being provided.
    /// NOTE: The server shouldn't give a charlie about what the format is, ever.
    /// The client gets told "this is markdown", it renders it, done. 
    /// </summary>
    public interface INoteItem
    {
        /// <summary>
        /// The identifier for this note item.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The title of the note being sent.
        /// </summary>
        public string NoteTitle { get; set; }

        /// <summary>
        /// The content type of the note (default is Markdown with additional shit)
        /// </summary>
        public MythicNoteContentType NoteContentType { get; set; }

        /// <summary>
        /// The type of the note.
        /// </summary>
        public INoteItemType NoteType { get; set; }

        /// <summary>
        /// The contents of the note.
        /// </summary>
        public string NoteContents { get; set; }

        /// <summary>
        /// The keywords for searching this content.
        /// </summary>
        public List<string> Tags { get; set; }
    }
}
