﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MythicTable.Notes.Data
{
    /// <summary>
    /// The provider for retrieving notes for the user.
    /// </summary>
    public interface INoteProvider
    {
        /// <summary>
        /// Gets all the notes for the user.
        /// </summary>
        /// <returns></returns>
        public Task<List<INoteItem>> GetAllNotes();

        /// <summary>
        /// Gets all the note types.
        /// </summary>
        /// <returns></returns>
        public Task<List<INoteItemType>> GetAllNoteTypes();
    }
}
