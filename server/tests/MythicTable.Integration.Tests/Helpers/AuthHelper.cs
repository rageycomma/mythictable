using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;

namespace MythicTable.Integration.TestUtils.Helpers
{
    public class TestStartup : Startup
    {
        public const string FAKE_USER_ID_HEADER = "FakeUserId";
        public const string FAKE_USER_NAME_HEADER = "FakeUserName";

        public TestStartup(IWebHostEnvironment environment, IConfiguration configuration): base(environment, configuration)
        {
        }

        protected override void ConfigureAuthentication(IServiceCollection services)
        {
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = "Test"; // has to match scheme in TestAuthenticationExtensions
                options.DefaultChallengeScheme = "Test";
            })
            .AddScheme<AuthenticationSchemeOptions, TestAuthHandler>("Test", options => { });
        }
    }

    public class TestAuthHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        public TestAuthHandler(IOptionsMonitor<AuthenticationSchemeOptions> options, 
            ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock)
            : base(options, logger, encoder, clock)
        {
        }

        protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, GetTestUserId()),
                new Claim("preferred_username", GetTestUserName())
            };
            var identity = new ClaimsIdentity(claims, "Test");
            var principal = new ClaimsPrincipal(identity);
            var ticket = new AuthenticationTicket(principal, "Test");

            var result = AuthenticateResult.Success(ticket);

            return Task.FromResult(result);
        }

        private string GetTestUserId()
        {
            if (Request.Headers.ContainsKey(TestStartup.FAKE_USER_ID_HEADER))
            {
                return Request.Headers[TestStartup.FAKE_USER_ID_HEADER];
            }
            return "Test user";
        }

        private string GetTestUserName()
        {
            if (Request.Headers.ContainsKey(TestStartup.FAKE_USER_NAME_HEADER))
            {
                return Request.Headers[TestStartup.FAKE_USER_NAME_HEADER];
            }
            return "test@user.me";
        }
    }
}