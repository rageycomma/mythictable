﻿using LocalDAOLib.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLib.DataProviders
{
    public interface IDataProvider<T> where T:IIndexable
    {
        IEnumerable<T> GetAll();
        T GetByID(int id);
        IEnumerable<T> Search(Func<T, bool> selector);
        void Store(T item);
        void Store(T[] items);
    }
}
