import { mount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';

import BaseModal from '@/core/components/BaseModal.vue';

const localVue = createLocalVue();
localVue.use(Vuex);

let wrapper;
let spy = jest.fn();

function buildWrapper(propsData) {
    spy.mockClear();
    let store = new Vuex.Store({
        modules: {
            window: {
                namespaced: true,
                mutations: {
                    setDisplayingModal: spy,
                },
            },
        },
    });

    wrapper = mount(BaseModal, {
        propsData,
        localVue,
        store,
        slots: { default: 'foobar' },
    });
}

describe('BaseModal', () => {
    it('should render if show is false', () => {
        buildWrapper({ show: false });
        expect(wrapper.find('.modal-mask').isVisible()).toBeTruthy();
    });
    it('should render if show is true', () => {
        buildWrapper({ show: true });
        expect(wrapper.find('.modal-mask').isVisible()).toBeTruthy();
    });
    it('should render slot contents', () => {
        buildWrapper({ show: true });
        expect(wrapper.find('.modal-mask').text()).toMatch('foobar');
    });
    it('Should set displayingModal to true, when mounted.', () => {
        buildWrapper({ show: true });
        expect(spy).toHaveBeenCalledWith(expect.anything(), true);
    });
    it('Should emit escape when the escape key is pressed.', async () => {
        buildWrapper({ show: true });
        await wrapper.find('.modal-container').trigger('keyup.esc');
        expect(wrapper.emitted('escape')).toBeTruthy();
    });
    it('Should set displayingModal to false, when destroyed.', () => {
        buildWrapper({ show: true });
        spy.mockClear();
        wrapper.destroy();
        expect(spy).toHaveBeenCalledWith(expect.anything(), false);
    });
});
